<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [test-stages](index.md) available for download.

## [0.2.0] - 2024-08-10

### Source tarball

- [test_stages-0.2.0.tar.gz](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.2.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.2.0.tar.gz.asc))

### Python wheel

- [test_stages-0.2.0-py3-none-any.whl](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.2.0-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.2.0-py3-none-any.whl.asc))

## [0.1.8] - 2024-05-25

### Source tarball

- [test_stages-0.1.8.tar.gz](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.8.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.8.tar.gz.asc))

### Python wheel

- [test_stages-0.1.8-py3-none-any.whl](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.8-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.8-py3-none-any.whl.asc))

## [0.1.7] - 2024-03-17

### Source tarball

- [test_stages-0.1.7.tar.gz](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.7.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.7.tar.gz.asc))

### Python wheel

- [test_stages-0.1.7-py3-none-any.whl](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.7-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.7-py3-none-any.whl.asc))

## [0.1.6] - 2024-02-08

### Source tarball

- [test_stages-0.1.6.tar.gz](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.6.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.6.tar.gz.asc))

### Python wheel

- [test_stages-0.1.6-py3-none-any.whl](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.6-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.6-py3-none-any.whl.asc))

## [0.1.5] - 2024-01-19

### Source tarball

- [test_stages-0.1.5.tar.gz](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.5.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.5.tar.gz.asc))

### Python wheel

- [test_stages-0.1.5-py3-none-any.whl](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.5-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.5-py3-none-any.whl.asc))

## [0.1.4] - 2023-12-19

### Source tarball

- [test_stages-0.1.4.tar.gz](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.4.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.4.tar.gz.asc))

### Python wheel

- [test_stages-0.1.4-py3-none-any.whl](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.4-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/test-stages/test_stages-0.1.4-py3-none-any.whl.asc))

[0.2.0]: https://gitlab.com/ppentchev/test-stages/-/tags/release%2F0.2.0
[0.1.8]: https://gitlab.com/ppentchev/test-stages/-/tags/release%2F0.1.8
[0.1.7]: https://gitlab.com/ppentchev/test-stages/-/tags/release%2F0.1.7
[0.1.6]: https://gitlab.com/ppentchev/test-stages/-/tags/release%2F0.1.6
[0.1.5]: https://gitlab.com/ppentchev/test-stages/-/tags/release%2F0.1.5
[0.1.4]: https://gitlab.com/ppentchev/test-stages/-/tags/release%2F0.1.4
