# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A `test-stages` implementation for the Tox test runner.

This module contains the configuration parsing and runtime glue to
let Tox run its test environments grouped in stages.
"""
