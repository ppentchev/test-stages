# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

contextlib-chdir >= 1, < 2; python_version < '3.11'
pyproject_hooks >= 1, < 2
tomli-w >= 1, < 2
tox >= 4, < 5
